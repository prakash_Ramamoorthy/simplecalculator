package calculator;
import action_listener.*;
import gui.SimpleCalculatorGui;
import math_operations.Math_operations;



public class DataFormatting {
	public static int i=0;
	public static double integerData=0;
	public static int integerArrayIndex=0;
	public static int decimalArrayIndex=0;
	public static double decimalData=0;
	public static double variablesArray[]={0,0};
	public static String operatorArray[];
	public static int noOfVariables=0;
	public static String operator;
	public static int digits = 0;
	public static boolean isDot=false;
	public static boolean singleOperator=false;
	 public static void dataInput(int input)
	{
		 SimpleCalculatorGui.txtLog.append(Integer.toString(input));
		 SimpleCalculatorGui.dummyTextArea.append(Integer.toString(input));
		 System.out.println("dummy text "+SimpleCalculatorGui.dummyTextArea.getText());
	}
	 
	
	public static void dataInput(String input)
	{
	System.out.println("input is"+input);
	if(input==".")
	{
	SimpleCalculatorGui.txtLog.append(input);
	 SimpleCalculatorGui.dummyTextArea.append(input);
	}
	else if(input=="=") 
	{
//		Double result =Math_operations.math(variablesArray, operator);
//		SimpleCalculatorGui.txtLog.setText(Double.toString(result));
		SimpleCalculatorGui.txtLog.append(operator);
		variablesArray[i]=Double.parseDouble(SimpleCalculatorGui.dummyTextArea.getText());
		System.out.println("variables 1 is "+variablesArray[1]);
		i=0;
		Double result =Math_operations.math(variablesArray, operator);
		SimpleCalculatorGui.txtLog.setText(Double.toString(result));
		variablesArray[i]=result;
		
	}
	else if(input!="=")
	{	
		System.out.println(Double.parseDouble(SimpleCalculatorGui.dummyTextArea.getText()));
		operator=input;
		SimpleCalculatorGui.txtLog.append(operator);
		variablesArray[i]=Double.parseDouble(SimpleCalculatorGui.dummyTextArea.getText());
		i++;
		SimpleCalculatorGui.dummyTextArea.setText(null);
		System.out.println("variables array "+variablesArray[0]);
	}
	
	
	}
	public static void backAndClear(int j,boolean isDot)
	{
		if(j==0)
		{	
			SimpleCalculatorGui.dummyTextArea.setText(null);
			i=0;
			getArrayIndex(0, isDot);
			variablesArray[0]=0;
			variablesArray[1]=0;
//			operatorArray={0,0};
			SimpleCalculatorGui.txtLog.setText(" ");
		}
		else
		{
			getArrayIndex(i, isDot);
		}
	}
	public static int getArrayIndex(int i,boolean DotOperator)
	{
		if(!DotOperator)
		{
			if(i==1)
				integerArrayIndex++;
			else if(i==-1)
				integerArrayIndex--;
			else if(i==0)
				integerArrayIndex=0;
			return integerArrayIndex;
		}
		else
		{
			if(i==1)
				decimalArrayIndex++;
			else if(i==-1)
				decimalArrayIndex--;
			else if(i==0)
				decimalArrayIndex=0;
			return decimalArrayIndex;
		}
	}
}

