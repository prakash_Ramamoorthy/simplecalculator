/* This program is to create a Graphical User Interface for Simple Calculator. */
package gui;
import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextArea;
import javax.swing.JSpinner;
import javax.swing.JTextArea;
import javax.swing.text.JTextComponent;

import action_listener.Action_Listener;

public class SimpleCalculatorGui {
	 public static  JTextArea txtLog;
	 public static JTextArea dummyTextArea;
	 public static JButton btn0;
	 public static JButton btn1;
	 public static JButton btn2;
	 public static JButton btn3;
	 public static JButton btn4;
	 public static JButton btn5;
	 public static JButton btn6;
	 public static JButton btn7;
	 public static JButton btn8;
	 public static JButton btn9;
	 public static JButton btnAdd;
	 public static JButton btnSub;
	 public static JButton btnMulti;
	 public static JButton btnDiv;
	 public static JButton btnEqual;
	 public static JButton btnBack;
	 public static JButton btnClr;
	 public static JButton btnDot;
	 public static JButton btnSifig;
	 public static JSpinner spinSifig;
	 public SimpleCalculatorGui()
	 {
//		 Creating the main frame for GUI
		 JFrame frame = new JFrame();
		 JPanel panel_1 = new JPanel();
		 frame.setContentPane(panel_1);
		 frame.setSize(224, 318);
		 frame.setTitle("Calculator");
		 frame.setResizable(true);
		 frame.setDefaultCloseOperation(3);

		 
		 panel_1.setLayout(new GridBagLayout());
		 panel_1.setVisible(true);
		 GridBagConstraints gBC = new GridBagConstraints();
		 
//		 text area to display the input and output
		 txtLog = new JTextArea();
		 gBC.weightx = 1.0;
		 gBC.weighty = 5.0;
	     gBC.fill = GridBagConstraints.BOTH;
	     gBC.gridheight = 1;
	     gBC.gridwidth = 4;
	     gBC.gridx = 0;
	     gBC.gridy = 0;
	     gBC.insets = new Insets(2,2,2,2);
//	     txtLog.setColumns(10);
//		 txtLog.setFont(new java.awt.Font("Tahoma", 0, 14));
	     txtLog.setBackground(Color.WHITE);
		 txtLog.setEditable(true);
		 panel_1.add(txtLog, gBC);
		 
		 dummyTextArea= new JTextArea();
		 
		 btnBack = new JButton("Backspace");
		 gBC.weightx = 1.0;
		 gBC.weighty = 1.0;
		 gBC.gridwidth = 3;
		 gBC.gridheight = 1;
	     gBC.gridx = 0;
	     gBC.gridy = 5;
	     gBC.fill = GridBagConstraints.BOTH;
//		 btnBack.setFont(new java.awt.Font("Tahoma", 1, 14));
		 panel_1.add(btnBack, gBC);
		 
		 
		 btnClr = new JButton("Clr");
		 gBC.weightx = 1.0;
		 gBC.gridwidth = 1;
	     gBC.gridx = 3;
	     gBC.gridy = 5;
//		 btnClr.setFont(new java.awt.Font("Tahoma", 1, 14));
		 panel_1.add(btnClr, gBC);
		 
		 btn7 = new JButton("7");
		 gBC.weightx = 1.0;
	     gBC.gridx = 0;
	     gBC.gridy = 6;
//		 btn7.setFont(new java.awt.Font("Tahoma", 1, 14));
		 panel_1.add(btn7, gBC);
		 
		 btn8 = new JButton("8");
		 gBC.weightx = 1.0;
	     gBC.gridx = 1;
	     gBC.gridy = 6;
//		 btn8.setFont(new java.awt.Font("Tahoma", 1, 14));
		 panel_1.add(btn8, gBC);

		 btn9 = new JButton("9");
		 gBC.weightx = 1.0;
	     gBC.gridx = 2;
	     gBC.gridy = 6;
//		 btn9.setFont(new java.awt.Font("Tahoma", 1, 14));
		 panel_1.add(btn9, gBC);
		 
		 btnDiv = new JButton("/");
		 gBC.weightx = 1.0;
	     gBC.gridx = 3;
	     gBC.gridy = 6;
//		 btnDiv.setFont(new java.awt.Font("Tahoma", 1, 14));
		 panel_1.add(btnDiv, gBC);
		 
		 btn4 = new JButton("4");
		 gBC.weightx = 1.0;
	     gBC.gridx = 0;
	     gBC.gridy = 7;
//		 btn4.setFont(new java.awt.Font("Tahoma", 1, 14));
		 panel_1.add(btn4, gBC);
		 
		 btn5 = new JButton("5");
		 gBC.weightx = 1.0;
	     gBC.gridx = 1;
	     gBC.gridy = 7;
//		 btn5.setFont(new java.awt.Font("Tahoma", 1, 14));
		 panel_1.add(btn5, gBC);

		 btn6 = new JButton("6");
		 gBC.weightx = 1.0;
	     gBC.gridx = 2;
	     gBC.gridy = 7;
//		 btn6.setFont(new java.awt.Font("Tahoma", 1, 14));
		 panel_1.add(btn6, gBC);
		 
		 btnMulti = new JButton("x");
		 gBC.weightx = 1.0;
	     gBC.gridx = 3;
	     gBC.gridy = 7;
//		 btnMulti.setFont(new java.awt.Font("Tahoma", 1, 14));
		 panel_1.add(btnMulti, gBC);
		 
		 btn1 = new JButton("1");
		 gBC.weightx = 1.0;
	     gBC.gridx = 0;
	     gBC.gridy = 8;
//		 btn1.setFont(new java.awt.Font("Tahoma", 1, 14));
		 panel_1.add(btn1, gBC);
		 
		 btn2 = new JButton("2");
		 gBC.weightx = 1.0;
	     gBC.gridx = 1;
	     gBC.gridy = 8;
//		 btn2.setFont(new java.awt.Font("Tahoma", 1, 14));
		 panel_1.add(btn2, gBC);

		 btn3 = new JButton("3");
		 gBC.weightx = 1.0;
	     gBC.gridx = 2;
	     gBC.gridy = 8;
//		 btn3.setFont(new java.awt.Font("Tahoma", 1, 14));
		 panel_1.add(btn3, gBC);
		 
		 btnSub = new JButton("-");
		 gBC.weightx = 1.0;
	     gBC.gridx = 3;
	     gBC.gridy = 8;
//		 btnSub.setFont(new java.awt.Font("Tahoma", 1, 14));
		 panel_1.add(btnSub, gBC);
		 
		 btn0 = new JButton("0");
		 gBC.weightx = 1.0;
	     gBC.gridx = 0;
	     gBC.gridy = 9;
//		 btn0.setFont(new java.awt.Font("Tahoma", 1, 14));
		 panel_1.add(btn0, gBC);
		 
		 btnDot = new JButton(".");
		 gBC.weightx = 1.0;
	     gBC.gridx = 1;
	     gBC.gridy = 9;
//		 btnDot.setFont(new java.awt.Font("Tahoma", 1, 14));
		 panel_1.add(btnDot, gBC);

		 btnEqual = new JButton("=");
		 gBC.weightx = 1.0;
	     gBC.gridx = 2;
	     gBC.gridy = 9;
//		 btnEqual.setFont(new java.awt.Font("Tahoma", 1, 14));
		 panel_1.add(btnEqual, gBC);
		 
		 btnAdd = new JButton("+");
		 gBC.weightx = 1.0;
	     gBC.gridx = 3;
	     gBC.gridy = 9;
//		 btnAdd.setFont(new java.awt.Font("Tahoma", 1, 14));
		 panel_1.add(btnAdd, gBC);
		 
		 frame.setVisible(true);
		 
		 SimpleCalculatorGui.btn0.addActionListener(new Action_Listener());
			SimpleCalculatorGui.btn1.addActionListener(new Action_Listener());
			SimpleCalculatorGui.btn2.addActionListener(new Action_Listener());
			SimpleCalculatorGui.btn3.addActionListener(new Action_Listener());
			SimpleCalculatorGui.btn4.addActionListener(new Action_Listener());
			SimpleCalculatorGui.btn5.addActionListener(new Action_Listener());
			SimpleCalculatorGui.btn6.addActionListener(new Action_Listener());
			SimpleCalculatorGui.btn7.addActionListener(new Action_Listener());
			SimpleCalculatorGui.btn8.addActionListener(new Action_Listener());
			SimpleCalculatorGui.btn9.addActionListener(new Action_Listener());
			SimpleCalculatorGui.btnAdd.addActionListener(new Action_Listener());
			SimpleCalculatorGui.btnSub.addActionListener(new Action_Listener());
			SimpleCalculatorGui.btnMulti.addActionListener(new Action_Listener());
			SimpleCalculatorGui.btnDiv.addActionListener(new Action_Listener());
			SimpleCalculatorGui.btnEqual.addActionListener(new Action_Listener());
			SimpleCalculatorGui.btnBack.addActionListener(new Action_Listener());
			SimpleCalculatorGui.btnClr.addActionListener(new Action_Listener());
			SimpleCalculatorGui.btnDot.addActionListener(new Action_Listener());
		 
		}
	 public static void main (String args[])
	 {
		 new SimpleCalculatorGui();
		 new Action_Listener();
	 }
}
